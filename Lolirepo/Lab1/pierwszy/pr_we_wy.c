/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <stdio.h>
#include <process.h>
#include <channel.h>
#include <misc.h>

int main()
{
	Channel*  KANAL2WY;
	Channel*  KANAL3WE;
	int suma;

	KANAL2WY   = (Channel *)  get_param (3);
	KANAL3WE   = (Channel *)  get_param (4);
	
	suma = 0;
	
	ChanOutInt(KANAL2WY, suma);
	
	suma = ChanInInt(KANAL3WE);
	
	printf("suma wynosi: %d\n", suma);

	exit_terminate (0);
	return 0;
}
