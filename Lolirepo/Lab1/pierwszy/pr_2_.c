/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

int main()
{
	Channel*  KANAL2WY;
	Channel*  KANAL3WE;
	Channel*  KANAL6WY;
	Channel*  KANAL7WE;
	int*      procNr;
	int suma;
	
	KANAL2WY   = (Channel *)  get_param (1);
	KANAL3WE   = (Channel *)  get_param (2);
	KANAL6WY   = (Channel *)  get_param (3);
	KANAL7WE   = (Channel *)  get_param (4);
	procNr     = (int *)      get_param (5);
	
	
	
	suma = ChanInInt(KANAL7WE) + *procNr;
	ChanOutInt(KANAL2WY, suma);

	return 0;
}
