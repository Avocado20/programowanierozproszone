Komunikat						Znaczenie 

PARASTATION 

1. Komunikat poprawnego uruchomienia

Using device atena.cs.put.poznan.pl:nap0
Transtech Iserver Version 2.50 17-Jun-92 
Copyright 1992 Transtech Parallel Systems Limited
Booting root transputer...
ok							--- �wiadczy o poprawnym zako�czeniu �adowania
								 kod�w proces�w
------------------------------------------------------------------------------------------------------
2. Komunikat niepoprawnego uruchomienia

Warning-imakef-isntab.inc-source file does not exist	--- Brak pliku opisuj�cego architektur� 								tandem node
							--- �le wybrany system uruchomieniowy dla 										przygotowanej aplikacji.
------------------------------------------------------------------------------------------------------

PARASTATION LUB TANDEMNODE

3. Komunikat niepoprawnej kompilacji

Brak pliku sequence_config.btl				--- Kompilacja nie powiod�a si� z powodu b��d�w.

-------------------------------------------------------------------------------------------------------
TANDEMNODE

4. Przyk�adowe niepoprawne uruchomienie systemu :

Open device=/dev/vxv1
Booting root transputer...ok
Serious-iccf- T.Node description file not included.
Brak pliku tymczasowego multi.wdg			--- nie wykonano translacji architektury
							projekt przeznaczony dla innego sytemu 										uruchomieniowego (PARASTATION)

-------------------------------------------------------------------------------------------------------
5. Przyk�adowe niepoprawne uruchomienie systemu:

Open device=/dev/vxv1
Booting root transputer...ok
iccf -- mixing OK					--- zako�czono translacj� architektury

Uruchamianie trwa...
Open device=/dev/vxv0
Booting root transputer...Pomoc
Uruchamianie programu przerwane				--- nie powiod�a si� inicjalizacja systemu 
								r�wnoleg�ego - 	sprz�t jest niedostepny

---------------------------------------------------------------------------------------------

6. Przyk�adowe poprawne uruchomienie systemu:

Open device=/dev/vxv1
Booting root transputer...ok
iccf -- mixing OK					--- zako�czono translacj� architektury

Uruchamianie trwa...
Open device=/dev/vxv0
Booting root transputer...ok
T.Reset V1.1 for Inmos
Copyright Telmat Informatique - December 1991

Reset.subsystem
Reset.subsystem						--- zako�czono inicjalizacj� systemu 
								r�wnoleg�ego

Uruchamianie trwa...
Open device=/dev/vxv0
Booting root transputer...ok
Running the T.Switcher program...

Using link 1 for system link.

T.Switcher2.2 V1.1 for INMOS

Copyright Telmat Informatique - Decembre 1991



Setting up network...
Switching finished.					--- zako�czono tworzenie po��cze�


Uruchamianie trwa...
Open device=/dev/vxv1
Booting root transputer...ok				--- zako�czenie �adowania kodu u�ytkownika
								dalsze komunikaty pochodz� od programu 
								u�ytkownika
------------------------------------------------------------------------------------------------------
7. B��dy uruchamiania

Uruchamianie programu przerwane		--- �wiadczy o b��dzie w programie u�ytkownika :
						- proces wyj�cia-wyj�cia nie wykona� operacji 
							exit_terminate
						- nieutworzony proces (np. z�a deklaracja pami�ci 									procesu)
						- proces usuni�ty w wyniku b��dnej operacji 
							(b��d dost�pu do pami�ci, dzielenie przez zero) 
						- oczekiwanie na niewygenerowany komunikat
						- oczekiwanie na odebranie komunikatu - brak odbiorcy
					--- lub zbyt kr�tkim dopuszczonym czasie przetwarzania kodu
						u�ytkownika: priorytety wysoki 60, niski 180 sekund 
						
Brak oczekiwanych komunikat�w  		--- proces wej�cia-wyj�cia nie wykona� operacji exit_terminate 
aplikacji u�ytkownika				zapewniaj�cej opr�nienie bufor�w komunikat�w we-wy

	
-------------------------------------------------------------------------------------------------------
8. Komunikat poprawny

Setting up network...
Error: CS.already.allocated to user.id=0, snode= 64
Error: CS.already.allocated to user.id=0, snode= 64
Error: CS.already.allocated to user.id=0, snode= 64
Error: CS.already.allocated to user.id=0, snode= 64
Switching finished.


POjawienie si� powy�szego komunikatu nie �wiadczy o niesprawno�ci systemu.
