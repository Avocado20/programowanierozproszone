/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "stale.h"

int main()
{
	Channel*  z_5;
	Channel*  do_9;
	Channel*  do_3;
	Channel*  do_obliczen;
	Channel*  z_obliczen;
	Channel*  do_5;
	Channel*  z_9;
	Channel*  z_3;
	
	int		dane[10000];
	int 	pozycja = 0;
	int		tmp = 0;
	int 	t1, t2, przesylanie[10];
	int 	wyniki_obl[6];
	int		z_9_obl[6];
	int		z_3_obl[6];
	int 	przesylanie_z3[8], przesylanie_z9[8];


	z_5      = (Channel *)  get_param (1);
	do_9     = (Channel *)  get_param (2);
	do_3     = (Channel *)  get_param (3);
	do_obliczen = (Channel *)  get_param (4);
	z_obliczen = (Channel *)  get_param (5);
	do_5     = (Channel *)  get_param (6);
	z_9      = (Channel *)  get_param (7);
	z_3      = (Channel *)  get_param (8);
	
	tmp = D6_1 + D3_1 + D9_1 + D2_p3_1;
	ChanIn( z_5, dane, sizeof(int)*tmp );
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[0] = t2 - t1;
	pozycja = pozycja + D6_1;
	t1 = ProcTime();
	ChanOut( do_9, dane + pozycja, sizeof(int)*D9_1);
	t2 = ProcTime();
	przesylanie[1] = t2 - t1;
	pozycja = pozycja + D3_1;
	t1 = ProcTime();
	ChanOut( do_3, dane + pozycja, sizeof(int)*D3_1 + D2_p3_1);
	t2 = ProcTime();
	przesylanie[2] = t2 - t1;
	pozycja = 0;
	
	tmp = D6_2 + D3_2 + D9_2;
	ChanIn( z_5, dane, sizeof(int)*tmp );
	ChanOutInt( do_obliczen, (int) dane );
	pozycja = pozycja + D6_2;
	t1 = ProcTime();
	ChanOut( do_9, dane + pozycja, sizeof(int)*D9_2);
	t2 = ProcTime();
	przesylanie[3] = t2 - t1;
	pozycja = pozycja + D3_2;
	t1 = ProcTime();
	ChanOut( do_3, dane + pozycja, sizeof(int)*D3_2 );
	t2 = ProcTime();
	przesylanie[4] = t2 - t1;
	pozycja = 0;
	
	tmp = D6_3 + D3_3 + D9_3 + D2_p3_3;
	ChanIn( z_5, dane, sizeof(int)*tmp );
	ChanOutInt( do_obliczen, (int) dane );
	pozycja = pozycja + D6_3;
	t1 = ProcTime();
	ChanOut( do_9, dane + pozycja, sizeof(int)*D9_3);
	t2 = ProcTime();
	przesylanie[5] = t2 - t1;
	pozycja = pozycja + D3_3;
	t1 = ProcTime();
	ChanOut( do_3, dane + pozycja, sizeof(int)*D3_3 + D2_p3_3 );
	t2 = ProcTime();
	przesylanie[6] = t2 - t1;
	pozycja = 0;
	
	tmp = D6_4 + D3_4 + D9_4;
	ChanIn( z_5, dane, sizeof(int)*tmp );
	ChanOutInt( do_obliczen, (int) dane );
	pozycja = pozycja + D6_4;
	t1 = ProcTime();
	ChanOut( do_9, dane + pozycja, sizeof(int)*D9_4);
	t2 = ProcTime();
	przesylanie[7] = t2 - t1;
	pozycja = pozycja + D3_4;
	t1 = ProcTime();
	ChanOut( do_3, dane + pozycja, sizeof(int)*D3_4 );
	t2 = ProcTime();
	przesylanie[8] = t2 - t1;
	pozycja = 0;

	ChanIn(z_obliczen, wyniki_obl, sizeof(int)*5);
	ChanIn(z_9, z_9_obl, sizeof(int)*6);
	ChanIn(z_3, z_3_obl, sizeof(int)*4);
	
	ChanOut(do_5, wyniki_obl, sizeof(int)*5);
	ChanOut(do_5, z_9_obl, sizeof(int)*6);
	ChanOut(do_5, z_3_obl, sizeof(int)*4);
	
	ChanIn(z_9, przesylanie_z9, sizeof(int)*5);
	ChanIn(z_3, przesylanie_z3, sizeof(int)*5);
	
	ChanOut(do_5, przesylanie, sizeof(int)*9);
	ChanOut(do_5, przesylanie_z9, sizeof(int)*5);
	ChanOut(do_5, przesylanie_z3, sizeof(int)*5);
	
	return 0;
}
