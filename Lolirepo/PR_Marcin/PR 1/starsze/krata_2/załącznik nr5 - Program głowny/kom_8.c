/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "stale.h"

int main()
{
	Channel*  z_5;
	Channel*  z_7;
	Channel*  do_7;
	Channel*  do_obliczen;
	Channel*  z_obliczen;
	Channel*  do_9;
	
	int		dane[10000];
	int 	wyniki_obl[6];
	int 	t1,t2;
	int 	przesylanie[5];

	z_5      = (Channel *)  get_param (1);
	z_7      = (Channel *)  get_param (2);
	do_7     = (Channel *)  get_param (3);
	do_obliczen = (Channel *)  get_param (4);
	z_obliczen = (Channel *)  get_param (5);
	do_9     = (Channel *)  get_param (6);
	
	ChanIn( z_5, dane, sizeof(int)*(D8_3 + D9_p8_3));
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[2] = t2 - t1;
	
	ChanIn( z_7, dane, sizeof(int)*D8_p7_3);
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[0] = t2 - t1;
	
	ChanIn( z_7, dane, sizeof(int)*D8_p7_4 );
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[1] = t2 - t1;
		
	ChanIn( z_5, dane, sizeof(int)*(D8_4 + D9_p8_4));
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[3] = t2 - t1;
	t1 = ProcTime();
	ChanOut( do_9, dane + D8_4, sizeof(int)*D9_p8_4 );
	t2 = ProcTime();
	przesylanie[4] = t2 - t1;
	
	ChanIn(z_obliczen, wyniki_obl, sizeof(int)*5);
	ChanOut( do_7, wyniki_obl, sizeof(int)*5 );
	ChanOut( do_7, przesylanie, sizeof(int)*5 );
	

	return 0;
}
