/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <stdio.h>
#include <process.h>
#include <channel.h>
#include <misc.h>
#include "stale.h"

int main()
{
	Channel*  z_4;
	Channel*  z_2;
	Channel*  do_obliczen;
	Channel*  z_obliczen;
	Channel*  do_4;
	Channel*  do_2;
	
	int		dane[10000];
	int 	pozycja = 0;
	int		tmp = 0, i;
	int 	wyniki_obl[6];
	int 	z_2_obl[6],	z_4_obl[6], z_7_obl[6], z_8_obl[6];
	int 	z_5_obl[6], z_6_obl[6], z_9_obl[6], z_3_obl[6];
	int		przesylanie_z6[10], przesylanie_z3[10], przesylanie_z9[10], przesylanie_z5[15];
	int		przesylanie_z7[10], przesylanie_z8[10], przesylanie_z4[10], przesylanie_z2[10];
	
	z_4      = (Channel *)  get_param (3);
	z_2      = (Channel *)  get_param (4);
	do_obliczen = (Channel *)  get_param (5);
	z_obliczen = (Channel *)  get_param (6);
	do_4     = (Channel *)  get_param (7);
	do_2     = (Channel *)  get_param (8);

	ChanIn( z_4, dane, sizeof(int)*D1_1);
	ChanOutInt( do_obliczen, (int) dane );
	
	ChanIn( z_4, dane, sizeof(int)*D1_2);
	ChanOutInt( do_obliczen, (int) dane );
	
	ChanIn( z_4, dane, sizeof(int)*D1_3);
	ChanOutInt( do_obliczen, (int) dane );
	
	ChanIn( z_4, dane, sizeof(int)*D1_4);
	ChanOutInt( do_obliczen, (int) dane );
	
	ChanIn( z_2, dane, sizeof(int)*D1_p2_4);
	ChanOutInt( do_obliczen, (int) dane );
	
	ChanIn( z_obliczen, wyniki_obl, sizeof(int)*6 );
	ChanIn( z_2, z_2_obl, sizeof(int)*5 );
	ChanIn( z_4, z_4_obl, sizeof(int)*5 );
	ChanIn( z_4, z_7_obl, sizeof(int)*4 );
	ChanIn( z_4, z_8_obl, sizeof(int)*5 );
	ChanIn( z_4, z_5_obl, sizeof(int) );
	ChanIn( z_4, z_6_obl, sizeof(int)*5 );
	ChanIn( z_4, z_9_obl, sizeof(int)*6 );
	ChanIn( z_4, z_3_obl, sizeof(int)*4 );
	
	ChanIn( z_2, przesylanie_z2, sizeof(int)*5 );
	ChanIn( z_4, przesylanie_z4, sizeof(int)*12 );
	ChanIn( z_4, przesylanie_z7, sizeof(int)*5 );
	ChanIn( z_4, przesylanie_z8, sizeof(int)*5 );
	ChanIn( z_4, przesylanie_z5, sizeof(int)*13 );
	ChanIn( z_4, przesylanie_z6, sizeof(int)*9 );
	ChanIn( z_4, przesylanie_z9, sizeof(int)*5 );
	ChanIn( z_4, przesylanie_z3, sizeof(int)*5 );

	for( i = 0 ;i < 6; i++ )
		printf("obl_1 : %d - %d \n", i+1, wyniki_obl[i]);
		
	for( i = 0 ;i < 5; i++)
		printf("z_2 : %d - %d \n", i+1, z_2_obl[i]);
	
	for(i = 0 ;i < 5; i++)
		printf("z_4 : %d - %d \n", i+1, z_4_obl[i]);
		
	for(i = 0 ;i < 4; i++)
		printf("z_7 : %d - %d \n", i+1, z_7_obl[i]);
		
	for(i = 0 ;i < 5; i++)
		printf("z_8 : %d - %d \n", i+1, z_8_obl[i]);
		
	for(i = 0 ;i < 1; i++)
		printf("z_5 : %d - %d \n", i+1, z_5_obl[i]);
		
	for(i = 0 ;i < 5; i++)
		printf("z_6 : %d - %d \n", i+1, z_6_obl[i]);
		
	for(i = 0 ;i < 6; i++)
		printf("z_9 : %d - %d \n", i+1, z_9_obl[i]);
		
	for(i = 0 ;i < 4; i++)
		printf("z_3 : %d - %d \n", i+1, z_3_obl[i]);
		
	printf("PRZESYLANIE \n");
	
	for( i = 0 ;i < 5; i++)
		printf("z_2 : %d - %d \n", i+1, przesylanie_z2[i]);
	
	for(i = 0 ;i < 12; i++)
		printf("z_4 : %d - %d \n", i+1, przesylanie_z4[i]);
		
	for(i = 0 ;i < 5; i++)
		printf("z_7 : %d - %d \n", i+1, przesylanie_z7[i]);
		
	for(i = 0 ;i < 5; i++)
		printf("z_8 : %d - %d \n", i+1, przesylanie_z8[i]);
		
	for(i = 0 ;i < 13; i++)
		printf("z_5 : %d - %d \n", i+1, przesylanie_z5[i]);
		
	for(i = 0 ;i < 9; i++)
		printf("z_6 : %d - %d \n", i+1, przesylanie_z6[i]);
		
	for(i = 0 ;i < 5; i++)
		printf("z_9 : %d - %d \n", i+1, przesylanie_z9[i]);
		
	for(i = 0 ;i < 5; i++)
		printf("z_3 : %d - %d \n", i+1, przesylanie_z3[i]);

	exit_terminate (0);
	return 0;
}
