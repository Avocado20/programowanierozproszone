/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "stale.h"

int main()
{
	Channel*  z_komunikacji;
	Channel*  do_komunikacji;
	
	int		*dane, wyniki[6];
	int 	i = 0, x, t1, t2;
	double 	W;
	int 	s,k, a;

	z_komunikacji = (Channel *)  get_param (1);
	do_komunikacji = (Channel *)  get_param (2);
	
	
	
	dane = (int*) ChanInInt(z_komunikacji);
	s = ProcTime();
	t1 = ProcTime();
	for( i = 0; i < D4_1; i++)
	{
		x = dane[i];
		W = 12.54*x*x*x  + 8.27*x;	
	}
	t2 =ProcTime();
	wyniki[0] = t2 - t1;
	
	dane = (int*) ChanInInt(z_komunikacji);
	t1 = ProcTime();
	for( i = 0; i < D4_2; i++)
	{
		x = dane[i];
		W = 12.54*x*x*x  + 8.27*x;	
	}
	t2 =ProcTime();
	wyniki[1] = t2 - t1;
	
	dane = (int*) ChanInInt(z_komunikacji);
	t1 = ProcTime();
	for( i = 0; i < D4_3; i++)
	{
		x = dane[i];
		W = 12.54*x*x*x  + 8.27*x;	
	}
	t2 =ProcTime();
	wyniki[2] = t2 - t1;
	
	dane = (int*) ChanInInt(z_komunikacji);
	t1 = ProcTime();
	for( i = 0; i < D4_4; i++)
	{
		x = dane[i];
		W = 12.54*x*x*x  + 8.27*x;	
	}
	t2 =ProcTime();
	wyniki[3] = t2 - t1;
	
	k = ProcTime();
	wyniki[4] = k - s;
	
	ChanOut(do_komunikacji,wyniki, sizeof(int)*5);
	
	return 0;
}
