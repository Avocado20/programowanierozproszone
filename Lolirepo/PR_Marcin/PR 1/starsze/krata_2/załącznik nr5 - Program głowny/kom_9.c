/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "stale.h"

int main()
{
	Channel*  z_6;
	Channel*  do_obliczen;
	Channel*  z_obliczen;
	Channel*  do_6;
	Channel*  z_8;
	
	int		dane[10000];
	int 	wyniki_obl[6];
	int 	t1,t2;
	int 	przesylanie[5];

	z_6      = (Channel *)  get_param (1);
	do_obliczen = (Channel *)  get_param (2);
	z_obliczen = (Channel *)  get_param (3);
	do_6     = (Channel *)  get_param (4);
	z_8     = (Channel *)  get_param (5);
	
	ChanIn( z_6, dane, sizeof(int)*D9_1);
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[0] = t2 - t1;
	
	ChanIn( z_6, dane, sizeof(int)*D9_2);
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[1] = t2 - t1;
	
	ChanIn( z_6, dane, sizeof(int)*D9_3);
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[2] = t2 - t1;
	
	ChanIn( z_6, dane, sizeof(int)*D9_4);
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[3] = t2 - t1;
	
	
	ChanIn( z_8, dane, sizeof(int)*D9_p8_4);
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[4] = t2 - t1;
		
	ChanIn(z_obliczen, wyniki_obl, sizeof(int)*6);
	ChanOut(do_6, wyniki_obl, sizeof(int)*6);
	ChanOut(do_6, przesylanie, sizeof(int)*5);

	
	return 0;
}
