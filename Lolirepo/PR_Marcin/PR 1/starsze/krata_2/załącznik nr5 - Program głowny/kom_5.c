/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "stale.h"

int main()
{
	Channel*  do_2;
	Channel*  do_4;
	Channel*  do_8;
	Channel*  do_6;
	Channel*  do_obliczen;
	Channel*  z_obliczen;
	Channel*  z_4;
	Channel*  z_6;
	
	int		dane[10000], przesylanie[15];
	int 	pozycja = 0;
	int		tmp = 0, i;
	int 	t1,t2;
	int 	wyniki_obl[6];
	int 	z_6_obl[6];
	int 	z_3_obl[6];
	int 	z_9_obl[6];
	int		przesylanie_z6[10], przesylanie_z3[10], przesylanie_z9[10];

	do_2        = (Channel *)  get_param (1);
	do_4        = (Channel *)  get_param (2);
	do_8        = (Channel *)  get_param (3);
	do_6        = (Channel *)  get_param (4);
	do_obliczen = (Channel *)  get_param (5);
	z_obliczen  = (Channel *)  get_param (6);
	z_4         = (Channel *)  get_param (7);
	z_6         = (Channel *)  get_param (8);
	
	for( i = 0; i < 10000; i++)
		dane[i] = rand();
	
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[0] = t2 - t1;
	pozycja = pozycja + D5;
	
	tmp = D4_1 + D7_1 + D1_1 + D8_p7_1;
	t1 = ProcTime();
	ChanOut( do_4, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[1] = t2 - t1;
	pozycja = pozycja + tmp;
	
	tmp = D6_1 + D3_1 + D9_1 + D2_p3_1;
	t1 = ProcTime();
	ChanOut( do_6, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[2] = t2 - t1;
	pozycja = pozycja + tmp;
	
	tmp = D4_2 + D7_2 + D1_2 + D8_p7_2;
	t1 = ProcTime();
	ChanOut( do_4, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[3] = t2 - t1;
	pozycja = pozycja + tmp;
	
	tmp = D6_2 + D3_2 + D9_2 + D2_p3_3;
	t1 = ProcTime();
	ChanOut( do_6, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[4] = t2 - t1;
	pozycja = pozycja + tmp;
	
	tmp = D4_3 + D7_3 + D1_3 + D8_p7_3;
	t1 = ProcTime();
	ChanOut( do_4, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[5] = t2 - t1;
	pozycja = pozycja + tmp;
	
	tmp = D6_3 + D3_3 + D9_3;
	t1 = ProcTime();
	ChanOut( do_6, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[6] = t2 - t1;
	pozycja = pozycja + tmp;
	
	tmp = D2_3;
	t1 = ProcTime();
	ChanOut( do_2, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[7] = t2 - t1;
	pozycja = pozycja + tmp;
	
	tmp = D8_3 + D9_p8_3;
	t1 = ProcTime();
	ChanOut( do_8, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[8] = t2 - t1;
	
	tmp = D4_4 + D7_4 + D1_4 + D8_p7_4;
	t1 = ProcTime();
	ChanOut( do_4, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[9] = t2 - t1;
	pozycja = pozycja + tmp;
	
	tmp = D6_4 + D3_4 + D9_4;
	t1 = ProcTime();
	ChanOut( do_6, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[10] = t2 - t1;
	pozycja = pozycja + tmp;
	
	tmp = D2_4 + D1_p2_4;
	t1 = ProcTime();
	ChanOut( do_2, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[11] = t2 - t1;
	pozycja = pozycja + tmp;
	
	tmp = D8_4 + D9_p8_4;
	t1 = ProcTime();
	ChanOut( do_8, dane + pozycja, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[13] = t2 - t1;
	
	ChanIn(z_obliczen, wyniki_obl, sizeof(int));
	ChanIn(z_6, z_6_obl, sizeof(int)*5);
	ChanIn(z_6, z_9_obl, sizeof(int)*6);
	ChanIn(z_6, z_3_obl, sizeof(int)*4);
	
	ChanOut(do_4, wyniki_obl, sizeof(int));
	ChanOut(do_4, z_6_obl, sizeof(int)*5);
	ChanOut(do_4, z_9_obl, sizeof(int)*6);
	ChanOut(do_4, z_3_obl, sizeof(int)*4);
	
	ChanIn(z_6, przesylanie_z6, sizeof(int)*9);
	ChanIn(z_6, przesylanie_z9, sizeof(int)*5);
	ChanIn(z_6, przesylanie_z3, sizeof(int)*5);
	
	ChanOut(do_4, przesylanie, sizeof(int)*13);
	ChanOut(do_4, przesylanie_z6, sizeof(int)*9);
	ChanOut(do_4, przesylanie_z9, sizeof(int)*5);
	ChanOut(do_4, przesylanie_z3, sizeof(int)*5);
	
	return 0;
}
