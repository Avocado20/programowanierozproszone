/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "stale.h"

int main()
{
	Channel*  z_komunikacji;
	Channel*  do_komunikacji;
	
	int		dane1[10000], wyniki;
	int 	*dane;
	int 	i = 0, x, t1, t2;
	double 	W;

	z_komunikacji = (Channel *)  get_param (1);
	do_komunikacji = (Channel *)  get_param (2);
	
	dane = (int*) ChanInInt(z_komunikacji);
	
	t1 = ProcTime();
	for( i = 0; i < D5; i++)
	{
		x = dane[i];
		W = 12.54*x*x*x  + 8.27*x;	
	}
	t2 =ProcTime();
	wyniki = t2 - t1;
	
	ChanOut(do_komunikacji,&wyniki, sizeof(int));

	return 0;
}
