/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "stale.h"

int main()
{
	Channel*  z_5;
	Channel*  do_1;
	Channel*  do_7;
	Channel*  do_obliczen;
	Channel*  z_obliczen;
	Channel*  do_5;
	Channel*  z_1;
	Channel*  z_7;
	
	int		dane[10000];
	int 	pozycja = 0;
	int		tmp = 0;
	int 	t1, t2, przesylanie[15];
	int 	wyniki_obl[6];
	int 	z_7_obl[6];
	int 	z_8_obl[6];
	int 	z_5_obl[6];
	int 	z_6_obl[6];
	int 	z_9_obl[6];
	int 	z_3_obl[6];
	int		przesylanie_z6[10], przesylanie_z3[10], przesylanie_z9[10], przesylanie_z5[15];
	int		przesylanie_z7[10], przesylanie_z8[10];

	z_5      = (Channel *)  get_param (1);
	do_1     = (Channel *)  get_param (2);
	do_7     = (Channel *)  get_param (3);
	do_obliczen = (Channel *)  get_param (4);
	z_obliczen = (Channel *)  get_param (5);
	do_5     = (Channel *)  get_param (6);
	z_1      = (Channel *)  get_param (7);
	z_7      = (Channel *)  get_param (8);
	
	tmp = D4_1 + D7_1 + D1_1 + D8_p7_1;
	t1 = ProcTime();
	ChanIn( z_5, dane, sizeof(int)*tmp );
	t2 = ProcTime();
	przesylanie[11] = t2 - t1;
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[0] = t2 - t1;
	pozycja = pozycja + D4_1;
	t1 = ProcTime();
	ChanOut( do_7, dane + pozycja, sizeof(int)*(D7_1 + D8_p7_1));
	t2 = ProcTime();
	przesylanie[1] = t2 - t1;
	pozycja = pozycja + D7_1 + D8_p7_1;
	t1 = ProcTime();
	ChanOut( do_1, dane + pozycja, sizeof(int)*D1_1 );
	t2 = ProcTime();
	przesylanie[2] = t2 - t1;
	pozycja = 0;
	

	tmp = D4_2 + D7_2 + D1_2 + D8_p7_2;
	ChanIn( z_5, dane, sizeof(int)*tmp );
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[3] = t2 - t1;
	pozycja = pozycja + D4_2;
	t1 = ProcTime();
	ChanOut( do_1, dane + pozycja, sizeof(int)*D1_2 );
	t2 = ProcTime();
	przesylanie[4] = t2 - t1;
	pozycja = 0;
	
	
	tmp = D4_3 + D7_3 + D1_3 + D8_p7_3;
	ChanIn( z_5, dane, sizeof(int)*tmp );
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[5] = t2 - t1;
	pozycja = pozycja + D4_3;
	t1 = ProcTime();
	ChanOut( do_1, dane + pozycja, sizeof(int)*D1_3 );
	t2 = ProcTime();
	przesylanie[6] = t2 - t1;
	pozycja = pozycja + D1_3;
	t1 = ProcTime();
	ChanOut( do_7, dane + pozycja, sizeof(int)*(D7_3 + D8_p7_3));
	t2 = ProcTime();
	przesylanie[7] = t2 - t1;
	pozycja = 0;
	t1 = ProcTime();
	
	tmp = D4_4 + D7_4 + D1_4 + D8_p7_4;
	ChanIn( z_5, dane, sizeof(int)*tmp );
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[8] = t2 - t1;
	pozycja = pozycja + D4_4;
	t1 = ProcTime();
	ChanOut( do_1, dane + pozycja, sizeof(int)*D1_4 );
	t2 = ProcTime();
	przesylanie[9] = t2 - t1;
	pozycja = pozycja + D1_4;
	t1 = ProcTime();
	ChanOut( do_7, dane + pozycja, sizeof(int)*(D7_4 + D8_p7_4 ));
	t2 = ProcTime();
	przesylanie[10] = t2 - t1;
	pozycja = 0;
	
	ChanIn(z_obliczen, wyniki_obl, sizeof(int)*5);
	ChanIn(z_7, z_7_obl, sizeof(int)*4);
	ChanIn(z_7, z_8_obl, sizeof(int)*5);
	ChanIn(z_5, z_5_obl, sizeof(int));
	ChanIn(z_5, z_6_obl, sizeof(int)*5);
	ChanIn(z_5, z_9_obl, sizeof(int)*6);
	ChanIn(z_5, z_3_obl, sizeof(int)*4);
	
	ChanOut(do_1, wyniki_obl, sizeof(int)*5);
	ChanOut(do_1, z_7_obl, sizeof(int)*4);
	ChanOut(do_1, z_8_obl, sizeof(int)*5);
	ChanOut(do_1, z_5_obl, sizeof(int));
	ChanOut(do_1, z_6_obl, sizeof(int)*5);
	ChanOut(do_1, z_9_obl, sizeof(int)*6);
	ChanOut(do_1, z_3_obl, sizeof(int)*4);
	
	
	ChanIn(z_7, przesylanie_z7, sizeof(int)*5);
	ChanIn(z_7, przesylanie_z8, sizeof(int)*5);
	ChanIn(z_5, przesylanie_z5, sizeof(int)*13);
	ChanIn(z_5, przesylanie_z6, sizeof(int)*9);
	ChanIn(z_5, przesylanie_z9, sizeof(int)*5);
	ChanIn(z_5, przesylanie_z3, sizeof(int)*5);
	
	ChanOut(do_1, przesylanie, sizeof(int)*12);
	ChanOut(do_1, przesylanie_z7, sizeof(int)*5);
	ChanOut(do_1, przesylanie_z8, sizeof(int)*5);
	ChanOut(do_1, przesylanie_z5, sizeof(int)*13);
	ChanOut(do_1, przesylanie_z6, sizeof(int)*9);
	ChanOut(do_1, przesylanie_z9, sizeof(int)*5);
	ChanOut(do_1, przesylanie_z3, sizeof(int)*5);
	
	return 0;
}
