/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "stale.h"

int main()
{
	Channel*  z_5;
	Channel*  do_1;
	Channel*  do_obliczen;
	Channel*  z_obliczen;
	Channel*  z_1;
	Channel*  z_3;
	
	int		dane[10000];
	int 	t1, t2, przesylanie[5];
	int 	wyniki_obl[6];

	z_5      = (Channel *)  get_param (1);
	do_1     = (Channel *)  get_param (2);
	do_obliczen = (Channel *)  get_param (3);
	z_obliczen = (Channel *)  get_param (4);
	z_1      = (Channel *)  get_param (5);
	z_3      = (Channel *)  get_param (6);

	ChanIn( z_3, dane, sizeof(int)*D2_p3_1);
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[0] = t2 - t1;
	
	ChanIn( z_5, dane, sizeof(int)*D2_3);
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[1] = t2 - t1;
	
	ChanIn( z_3, dane, sizeof(int)*D2_p3_3);
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[2] = t2 - t1;
		
	ChanIn( z_5, dane, sizeof(int)*(D2_4 + D1_p2_4) );
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[3] = t2 - t1;
	
	t1 = ProcTime();
	ChanOut( do_1, dane, sizeof(int)*D1_p2_4);
	t2 = ProcTime();
	przesylanie[4] = t2 - t1;
	
	ChanIn( z_obliczen, wyniki_obl, sizeof(int)*5 );
	ChanOut( do_1, wyniki_obl, sizeof(int)*5 );
	ChanOut( do_1, przesylanie, sizeof(int)*5 );
	
	return 0;
}
