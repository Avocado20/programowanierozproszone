/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "stale.h"

int main()
{
	Channel*  z_4;
	Channel*  do_8;
	Channel*  do_obliczen;
	Channel*  z_obliczen;
	Channel*  do_4;
	Channel*  z_8;
	
	int		dane[10000];
	int 	t1, t2;
	int 	wyniki_obl[6];
	int 	z_8_obl[6];
	int 	przesylanie[5], przesylanie_z8[5];

	z_4      = (Channel *)  get_param (1);
	do_8     = (Channel *)  get_param (2);
	do_obliczen = (Channel *)  get_param (3);
	z_obliczen = (Channel *)  get_param (4);
	do_4     = (Channel *)  get_param (5);
	z_8      = (Channel *)  get_param (6);
	
	ChanIn( z_4, dane, sizeof(int)*(D7_1 + D8_p7_1));
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[0] = t2 - t1;
	
	ChanIn( z_4, dane, sizeof(int)*(D7_3 + D8_p7_3));
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[1] = t2 - t1;
	t1 = ProcTime();
	ChanOut( do_8, dane, sizeof(int)*D8_p7_3);
	t2 = ProcTime();
	przesylanie[2] = t2 - t1;
	
	ChanIn( z_4, dane, sizeof(int)*(D7_4 + D8_p7_4 ));
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[3] = t2 - t1;
	t1 = ProcTime();
	ChanOut( do_8, dane, sizeof(int)*D8_p7_4 );
	t2 = ProcTime();
	przesylanie[4] = t2 - t1;
	
	ChanIn(z_obliczen, wyniki_obl, sizeof(int)*4);
	ChanIn(z_8, z_8_obl, sizeof(int)*5);
	ChanOut( do_4, wyniki_obl, sizeof(int)*4);
	ChanOut( do_4, z_8_obl, sizeof(int)*5);
	
	ChanIn(z_8, przesylanie_z8, sizeof(int)*5);
	ChanOut( do_4, przesylanie, sizeof(int)*5);
	ChanOut( do_4, przesylanie_z8, sizeof(int)*5);
	
	return 0;
}
