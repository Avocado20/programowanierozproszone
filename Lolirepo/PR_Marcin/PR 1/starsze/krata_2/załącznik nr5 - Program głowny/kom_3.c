/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "stale.h"

int main()
{
	Channel*  z_6;
	Channel*  do_obliczen;
	Channel*  z_obliczen;
	Channel*  do_6;
	Channel*  do_2;
	
	int		dane[10000];
	int 	wyniki_obl[6];
	int 	t1,t2;
	int 	przesylanie[5];

	z_6      = (Channel *)  get_param (1);
	do_obliczen = (Channel *)  get_param (2);
	z_obliczen = (Channel *)  get_param (3);
	do_6     = (Channel *)  get_param (4);
	do_2     = (Channel *)  get_param (5);
	
	ChanIn( z_6, dane, sizeof(int)*D3_1 + D2_p3_1 );
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[1] = t2 - t1;
	
	ChanIn( z_6, dane, sizeof(int)*D3_2);
	t1 = ProcTime();
	ChanOut( do_2, dane, sizeof(int)*D2_p3_1 );
	t2 = ProcTime();
	przesylanie[0] = t2 - t1;
	
	ChanIn( z_6, dane, sizeof(int)*D3_3 + D2_p3_3 );
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[2] = t2 - t1;
	t1 = ProcTime();
	ChanOut( do_2, dane, sizeof(int)*D2_p3_3);
	t2 = ProcTime();
	przesylanie[3] = t2 - t1;
	
	ChanIn( z_6, dane, sizeof(int)*D3_4);
	t1 = ProcTime();
	ChanOutInt( do_obliczen, (int) dane );
	t2 = ProcTime();
	przesylanie[4] = t2 - t1;
	
	ChanIn(z_obliczen, wyniki_obl, sizeof(int)*4 );
	ChanOut(do_6, wyniki_obl, sizeof(int)*4);
	ChanOut(do_6, przesylanie, sizeof(int)*5);

	return 0;
}
