#include <stdio.h>
#include <misc.h>
#include <channel.h>
#include <process.h>

int main ()

{
	Channel*	from_glowny;
	Channel*	to_glowny;
	
	int odebrane[10000];
	int ilosc = 10000;
	int i,j,x;
	int wyniki_obliczen[19];
	int czas1, czas2;
	float W;
	
	from_glowny = (Channel *) get_param (1);
	to_glowny = (Channel *) get_param (2);
	
	ChanIn( from_glowny, odebrane, sizeof(int)*ilosc );
		
	for (i = 0; i < 19; i++) {
	
		czas1 = ProcTime();
		for (j = 0; j < ilosc; j++)
		{
			x = odebrane[i];
			W = 12.54*x*x*x  + 8.27*x;
		}
		czas2 = ProcTime();
		wyniki_obliczen[i] = czas2 - czas1;
		ilosc += 500;
	}
	
	ChanOut( to_glowny, wyniki_obliczen, sizeof(int)*19 );
	

return 0;
}
