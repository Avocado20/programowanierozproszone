#include <stdio.h>
#include <misc.h>
#include <channel.h>
#include <process.h>
#include <stdlib.h>

 
int main ()
{
	Channel*	to_poziom;
	Channel*	to_pion;
	Channel*	from_poziom;
	Channel*	from_pion;
	
	int		i, ilosc = 10000;
	int 	wynik1[20], wynik2[20];
	int		dane[10000];

	to_poziom = (Channel *) get_param (3);
	to_pion = (Channel *) get_param (4);
	from_poziom = (Channel *) get_param (5);
	from_pion   = (Channel *) get_param (6);
	
	for( i = 0; i < ilosc; i++)
		dane[i] = rand();
	
	ChanOut( to_poziom, dane, sizeof(int)*ilosc );
	ChanOut( to_pion, dane, sizeof(int)*ilosc );
	
	ChanIn( from_poziom, wynik1, sizeof(int)*19 );
	ChanIn( from_pion, wynik2, sizeof(int)*19 );
	
	printf("Od procesora wolniejszego : \n");
	for( i = 0; i < 19; i++)
		printf("%d \n", wynik1[i]);
		
	printf("Od procesora szybszego : \n");
	for( i = 0; i < 19; i++)
		printf("%d \n", wynik2[i]);

	  
exit_terminate (0);
return 0;

}
