#include <stdio.h>
#include <misc.h>
#include <channel.h>
#include <process.h>
#include <stdlib.h>

 
int main ()
{
	Channel*	to_poziom;
	Channel*	to_pion;
	int		i, ilosc = 1000;
	int 	czas1, czas2;
	int 	wynik1[20], wynik2[20];
	int		dane[10000];

	to_poziom   = (Channel *) get_param (3);
	to_pion   = (Channel *) get_param (4);
	
	printf("Rozpoczete Wysylanie.\n");
	
	for( i = 1; i < 10000; i++)
		dane[i] = rand();
	
	for (i = 0; i < 19; i++)   {
		
		czas1 = ProcTime();
		ChanOut( to_poziom, dane, sizeof(int)*ilosc );
		czas2  = ProcTime();
		wynik1[i] = czas2 - czas1;
		printf(" i = %d, ilosc = %d.\n", i, ilosc);
		ilosc += 500;
	}
	
	printf("Zakonczone wysylanie w poziomie. \n");
	
	ilosc = 1000;
	
	for (i = 0; i < 19; i++)   {
		
		czas1 = ProcTime();
		ChanOut( to_pion, dane, sizeof(int)*ilosc );
		czas2  = ProcTime();
		wynik2[i] = czas2 - czas1;
		ilosc += 500;
	}
	
	printf("Zakonczone wysylanie w pionie \n");
	
	ilosc = 1000;

		for (i = 0; i < 19; i++)   {
		
		printf("%d\n", wynik1[i]);
		ilosc += 500;
	}
	
	ilosc = 1000;
	printf("\n\n\n");

	for (i = 0; i < 19; i++)   {
		
		printf("%d\n", wynik2[i]);
		ilosc += 500;
	}



	  
exit_terminate (0);
return 0;

}
