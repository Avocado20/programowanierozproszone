/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"


int main()
{
	Channel*  KANAL6WE;
	Channel*  KANAL5WY;
	

	int tablica[100000];
	int size,i, eval, time, time2;

	KANAL6WE   = (Channel *)  get_param (1);
	KANAL5WY   = (Channel *)  get_param (2);

	
	
	ChanIn(KANAL6WE,tablica,4*100000);

	size = ChanInInt(KANAL6WE);	

	while (size != 0){
	
	time = ProcTime();
	for (i = 0 ; i <size; i++)
		eval = compute(tablica[i]);
	time2 = ProcTime();
	ChanOutInt(KANAL5WY,time2-time);
    size = ChanInInt(KANAL6WE);	
	}
	

	return 0;
}
