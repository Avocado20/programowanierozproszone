/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

int main()
{
	Channel*  KANAL6WE;
	Channel*  KANAL5WY;
	Channel*  KANAL2WY;
	Channel*  KANAL3WE;

	int* tablica;
	int size;
	int time,time2;

	KANAL6WE   = (Channel *)  get_param (1);
	KANAL5WY   = (Channel *)  get_param (2);
	KANAL2WY   = (Channel *)  get_param (3);
	KANAL3WE   = (Channel *)  get_param (4);
	
	
	
	
	tablica = (int*) ChanInInt(KANAL6WE);	

	ChanOut(KANAL2WY,tablica,4*100000);

	for (size = 5000; size <= 5000; size+=5000){

            ChanOutInt(KANAL2WY,size);            
            time = 	ChanInInt(KANAL3WE);
    	    ChanOutInt(KANAL5WY,size);
    	    ChanOutInt(KANAL5WY,time);

	}

	ChanOutInt(KANAL5WY,0);
	ChanOutInt(KANAL2WY,0);

	

	return 0;
}
