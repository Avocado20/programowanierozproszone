/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"

int main()
{
	Channel*  KANAL0WE;
	Channel*  KANAL7WY;

	int tablica[100000];
	int size, i, eval, time, time2;

	KANAL0WE   = (Channel *)  get_param (1);
	KANAL7WY   = (Channel *)  get_param (2);

	
	
	ChanIn(KANAL0WE,tablica,4*100000);

	
	size = ChanInInt(KANAL0WE);	

	while (size != 0){
	
	time = ProcTime();
	for (i = 0 ; i <size; i++)
		eval = compute(tablica[i]);
	time2 = ProcTime();
	ChanOutInt(KANAL7WY,time2-time);
	size = ChanInInt(KANAL0WE);	
	}

	
	return 0;
}
