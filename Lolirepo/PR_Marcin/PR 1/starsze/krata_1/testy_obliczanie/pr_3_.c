/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

int main()
{
	Channel*  KANAL0WE;
	Channel*  KANAL7WY;
	Channel*  KANAL4WY;
	Channel*  KANAL5WE;

	int* tablica;
	int size;
	int time,time2;

	KANAL0WE   = (Channel *)  get_param (1);
	KANAL7WY   = (Channel *)  get_param (2);
	KANAL4WY   = (Channel *)  get_param (3);
	KANAL5WE   = (Channel *)  get_param (4);

	
	
	
	tablica = (int*) ChanInInt(KANAL0WE);	

	ChanOut(KANAL4WY,tablica,4*100000);

	for (size = 5000; size <= 100000; size+=5000){
	ChanOutInt(KANAL4WY,size);
	
	time = 	ChanInInt(KANAL5WE);

	ChanOutInt(KANAL7WY,size);
	ChanOutInt(KANAL7WY,time);
	}
	
	ChanOutInt(KANAL4WY,0);
	ChanOutInt(KANAL7WY,0);
	


	return 0;
}
