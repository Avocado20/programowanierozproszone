/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <stdio.h>
#include <process.h>
#include <channel.h>
#include <misc.h>

int main()
{
	Channel*  KANAL2WY;
	Channel*  KANAL3WE;
	Channel*  KANAL4WY;
	Channel*  KANAL5WE;

	int tablica[100000];
	int size, i;
	int time;

	KANAL2WY   = (Channel *)  get_param (3);
	KANAL3WE   = (Channel *)  get_param (4);
	KANAL4WY   = (Channel *)  get_param (5);
	KANAL5WE   = (Channel *)  get_param (6);


	for (i = 0; i < 100000; i++){
		tablica[i] = 84;
	}
	ChanOutInt(KANAL2WY,(int)tablica);

	printf("Wyniki z poziomu:\n");

	
	size = ChanInInt(KANAL3WE);

	while (size != 0){
	time = ChanInInt(KANAL3WE);
	printf("%d, %d\n",size,time);
	size = ChanInInt(KANAL3WE);
	}

	ChanOutInt(KANAL4WY,(int)tablica);
	
	printf("Wyniki z pionu:\n");
	size = ChanInInt(KANAL5WE);

	while (size != 0){
	time = ChanInInt(KANAL5WE);
	printf("%d, %d\n",size,time);
	size = ChanInInt(KANAL5WE);
	}

	exit_terminate (0);
	return 0;




}
