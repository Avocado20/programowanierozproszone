/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "common.h"

int main()
{
	Channel*  KANAL0WE;
	Channel*  KANAL7WY;
	Channel*  KANAL2WE;
	Channel*  KANAL3WE;

	int table[200000], i, eval, sendTime, computeTime;

	struct time_data data;


	KANAL0WE   = (Channel *)  get_param (1);
	KANAL7WY   = (Channel *)  get_param (2);
	KANAL2WE   = (Channel *)  get_param (3);
	KANAL3WE   = (Channel *)  get_param (4);

	sendTime = ProcTime();
	ChanIn(KANAL0WE,table,4*ALPHA_C);
	sendTime = ProcTime() - sendTime;

	computeTime = ProcTime();
	for (i = 0; i < ALPHA_C; i++)
		eval = compute(table[i]);
	computeTime = ProcTime() - computeTime;

	

	data.time = sendTime;
	data.sender = 'B';
	data.reciever = 'C';
	data.processor = 'C';
	data.type = SEND;
	ChanOut(KANAL7WY,&data,sizeof(struct time_data));

	data.time = computeTime;
	data.processor = 'C';
	data.type = COMPUTE;
	ChanOut(KANAL7WY,&data,sizeof(struct time_data));

	ChanIn(KANAL2WE,&data,sizeof(struct time_data));
	ChanOut(KANAL7WY,&data,sizeof(struct time_data));

	ChanIn(KANAL2WE,&data,sizeof(struct time_data));
	ChanOut(KANAL7WY,&data,sizeof(struct time_data));

	ChanIn(KANAL2WE,&data,sizeof(struct time_data));
	ChanOut(KANAL7WY,&data,sizeof(struct time_data));

	ChanIn(KANAL2WE,&data,sizeof(struct time_data));
	ChanOut(KANAL7WY,&data,sizeof(struct time_data));
	
	ChanIn(KANAL2WE,&data,sizeof(struct time_data));
	ChanOut(KANAL7WY,&data,sizeof(struct time_data));
	
	ChanIn(KANAL2WE,&data,sizeof(struct time_data));
	ChanOut(KANAL7WY,&data,sizeof(struct time_data));

	ChanIn(KANAL2WE,&data,sizeof(struct time_data));
	ChanOut(KANAL7WY,&data,sizeof(struct time_data));

	ChanIn(KANAL2WE,&data,sizeof(struct time_data));
	ChanOut(KANAL7WY,&data,sizeof(struct time_data));
	return 0;
}
