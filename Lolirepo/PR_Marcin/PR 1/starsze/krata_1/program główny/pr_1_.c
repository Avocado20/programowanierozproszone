/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include <stdlib.h>

#include "common.h"

int main()
{
	Channel*  KANAL6WY;
	Channel*  KANAL5WE;
	Channel*  KANAL7WY;
	Channel*  KANAL0WE;
	Channel*  KANAL2WY;
	Channel*  KANAL3WE;
	Channel*  KANAL4WY;
	Channel*  KANAL1WY;

	struct time_data *data;

	int timeB = 0, timeC = 0, i;


	int table[800000];

	KANAL6WY   = (Channel *)  get_param (1);
	KANAL5WE   = (Channel *)  get_param (2);
	KANAL7WY   = (Channel *)  get_param (3);
	KANAL0WE   = (Channel *)  get_param (4);
	KANAL2WY   = (Channel *)  get_param (5);
	KANAL3WE   = (Channel *)  get_param (6);
	KANAL4WY   = (Channel *)  get_param (7);
	KANAL1WY   = (Channel *)  get_param (8);
	
	data = malloc(sizeof(struct time_data));

	for (i = 0; i < 800000; i++){
		table[i] = 84;
	}
	/*wysylamy wskazniki*/
	ChanOutInt(KANAL7WY,(int)table);
	ChanOutInt(KANAL4WY,(int)table);
	ChanOutInt(KANAL1WY,(int)table);
	ChanOutInt(KANAL2WY,(int)table);

	timeB = ProcTime();
	ChanOut(KANAL6WY,&table[ALPHA_A],4*ALPHA_B);
	timeB = ProcTime() - timeB;

	timeC = ProcTime();
	ChanOut(KANAL6WY,&table[ALPHA_A + ALPHA_B],4*ALPHA_C);
	timeC = ProcTime() - timeC;

	data->time = timeB;
	data->sender = 'A';
	data->reciever = 'B';
	data->processor = 'B';
	data->type = SEND;

	ChanOut(KANAL6WY,data,sizeof(struct time_data));
	
	data->time = timeC;
	data->sender = 'A';
	data->reciever = 'B';
	data->processor = 'C';
	data->type = SEND;

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	/*odbieramy dane o obliczeniach*/
	ChanIn(KANAL0WE,data,sizeof(struct time_data));

	/*wysylamy dane o obliczeniach*/
	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL3WE,data,sizeof(struct time_data));
	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL3WE,data,sizeof(struct time_data));
	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL3WE,data,sizeof(struct time_data));
	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL3WE,data,sizeof(struct time_data));
	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL3WE,data,sizeof(struct time_data));
	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL3WE,data,sizeof(struct time_data));
	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL3WE,data,sizeof(struct time_data));
	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL3WE,data,sizeof(struct time_data));
	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	return 0;
}
