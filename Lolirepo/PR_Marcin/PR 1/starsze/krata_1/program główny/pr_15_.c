/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"

int main()
{
	Channel*  KANAL5WE;
	Channel*  KANAL4WY;

	int *table, time, i, eval;
	struct time_data data;

	KANAL5WE   = (Channel *)  get_param (1);
	KANAL4WY   = (Channel *)  get_param (2);

	table = (int*) ChanInInt(KANAL5WE);
	
	time = ProcTime();
    for (i = 0; i <ALPHA_H; i++)
		eval = compute(table[i]);
	time = ProcTime() - time;

	data.time = time;
	data.processor = 'H';
	data.type = COMPUTE;
	ChanOut(KANAL4WY,&data,sizeof(struct time_data));

	return 0;
}
