/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"
int main()
{
	Channel*  KANAL0WE;
	Channel*  KANAL7WY;
	Channel*  KANAL2WY;
	Channel*  KANAL3WE;
	Channel*  KANAL5WE;
	Channel*  KANAL4WY;
	Channel*  KANAL6WY;
	Channel*  KANAL1WY;

	int table[300000], timeD, timeE, timeToE,aaa;
	struct time_data data;


	KANAL0WE   = (Channel *)  get_param (1);
	KANAL7WY   = (Channel *)  get_param (2);
	KANAL2WY   = (Channel *)  get_param (3);
	KANAL3WE   = (Channel *)  get_param (4);
	KANAL5WE   = (Channel *)  get_param (5);
	KANAL4WY   = (Channel *)  get_param (6);
	KANAL6WY   = (Channel *)  get_param (7);
	KANAL1WY   = (Channel *)  get_param (8);

	timeD = ProcTime();
	ChanIn(KANAL0WE,table,4*ALPHA_D);
	timeD = ProcTime() - timeD;

	ChanOutInt(KANAL4WY,(int)table);

	timeE = ProcTime();
	ChanIn(KANAL0WE,&table[ALPHA_D],4*ALPHA_E);
	timeE = ProcTime() - timeE;

	timeToE = ProcTime();
	ChanOut(KANAL2WY,&table[ALPHA_D],4*ALPHA_E);
	timeToE = ProcTime() - timeToE;

	

	data.time = timeD;
	data.sender = 'A';
	data.reciever = 'D';
	data.processor = 'D';
	data.type = SEND;
	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	data.time = timeE;
	data.sender = 'A';
	data.reciever = 'D';
	data.processor = 'E';
	data.type = SEND;
	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	data.time = timeToE;
	data.sender = 'D';
	data.reciever = 'E';
	data.processor = 'E';
	data.type = SEND;
	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL5WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL0WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL0WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	return 0;
}
