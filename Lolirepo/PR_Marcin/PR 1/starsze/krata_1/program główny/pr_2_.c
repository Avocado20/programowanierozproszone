/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"

int main()
{
	Channel*  KANAL0WY;
	Channel*  KANAL7WE;
	Channel*  KANAL5WE;

	struct time_data data;

	int timeF = 0, timeG = 0, *table;

	KANAL0WY   = (Channel *)  get_param (1);
	KANAL7WE   = (Channel *)  get_param (2);
	KANAL5WE   = (Channel *)  get_param (3);

	table = (int*) ChanInInt(KANAL5WE);

	timeF = ProcTime();
	ChanOut(KANAL0WY,&table[ALPHA_A + ALPHA_B +ALPHA_C + ALPHA_D + ALPHA_E], 4*ALPHA_F);
	timeF = ProcTime() - timeF;

	timeG = ProcTime();
	ChanOut(KANAL0WY,&table[ALPHA_A + ALPHA_B +ALPHA_C+ALPHA_D + ALPHA_E + ALPHA_F], 4*ALPHA_G);
	timeG = ProcTime() - timeG;

	data.time = timeF;
	data.sender = 'A';
	data.reciever = 'F';
	data.processor = 'F';
	data.type = SEND;
	ChanOut(KANAL0WY,&data,sizeof(struct time_data));

	data.time = timeG;
	data.sender = 'A';
	data.reciever = 'F';
	data.processor = 'G';
	data.type = SEND;
	ChanOut(KANAL0WY,&data,sizeof(struct time_data));

	return 0;
}
