/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"

int main()
{
	Channel*  KANAL6WE;
	Channel*  KANAL5WY;

	int table[200000], i, eval, sendTime, computeTime;

	struct time_data data;

	KANAL6WE   = (Channel *)  get_param (1);
	KANAL5WY   = (Channel *)  get_param (2);
	
	sendTime = ProcTime();
	ChanIn(KANAL6WE,table,4*ALPHA_E);
	sendTime = ProcTime() - sendTime;

	computeTime = ProcTime();
	for (i = 0 ; i < ALPHA_E; i++)
		eval = compute(table[i]);
	computeTime = ProcTime() - computeTime;

	data.time = sendTime;
	data.sender = 'D';
	data.reciever = 'E';
	data.processor = 'E';
	data.type = SEND;
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	data.time = computeTime;
	data.processor = 'E';
	data.type = COMPUTE;
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));


	return 0;
}
