/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include <stdlib.h>

#include "common.h"

int main()
{
	Channel*  KANAL2WE;
	Channel*  KANAL3WY;
	Channel*  KANAL6WY;
	Channel*  KANAL7WE;
	Channel*  KANAL4WY;
	Channel*  KANAL5WE;

	struct time_data *data;

	int table[400000];

	int timeB = 0,timeC = 0, timeToC = 0;

	KANAL2WE   = (Channel *)  get_param (1);
	KANAL3WY   = (Channel *)  get_param (2);
	KANAL6WY   = (Channel *)  get_param (3);
	KANAL7WE   = (Channel *)  get_param (4);
	KANAL4WY   = (Channel *)  get_param (5);
	KANAL5WE   = (Channel *)  get_param (6);

	data = malloc(sizeof(struct time_data));
	
	timeB = ProcTime();
	ChanIn(KANAL2WE,table,4*ALPHA_B);
	timeB = ProcTime() - timeB;
	
	ChanOutInt(KANAL6WY,(int)table);

	timeC = ProcTime();
	ChanIn(KANAL2WE,&table[ALPHA_B],4*ALPHA_C);
	timeC = ProcTime() - timeC;

	timeToC = ProcTime();
	ChanOut(KANAL4WY,&table[ALPHA_B],4*ALPHA_C);
	timeToC = ProcTime() - timeToC;
	

	data->time = timeB;
	data->sender = 'A';
	data->reciever = 'B';
	data->processor = 'B';
	data->type = SEND;
	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	data->time = timeC;
	data->sender = 'A';
	data->reciever = 'B';
	data->processor = 'C';
	data->type = SEND;
	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	data->time = timeToC;
	data->sender = 'B';
	data->reciever = 'C';
	data->processor = 'C';
	data->type = SEND;
	ChanOut(KANAL6WY,data,sizeof(struct time_data));


	ChanIn(KANAL2WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));
	
	ChanIn(KANAL2WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL2WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL5WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL5WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL5WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL5WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL5WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL5WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL5WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL5WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL5WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL5WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL2WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL2WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL2WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL2WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL2WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL2WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL2WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));

	ChanIn(KANAL2WE,data,sizeof(struct time_data));

	ChanOut(KANAL6WY,data,sizeof(struct time_data));


	return 0;
}
