/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"

int main()
{
	Channel*  KANAL4WE;
	Channel*  KANAL5WY;
	Channel*  KANAL6WY;
	Channel*  KANAL7WE;
	Channel*  KANAL1WY;
	Channel*  KANAL2WE;

	int table[300000], timeF, timeG, timeToG;
	struct time_data data;

	KANAL4WE   = (Channel *)  get_param (1);
	KANAL5WY   = (Channel *)  get_param (2);
	KANAL6WY   = (Channel *)  get_param (3);
	KANAL7WE   = (Channel *)  get_param (4);
	KANAL1WY   = (Channel *)  get_param (5);
	KANAL2WE   = (Channel *)  get_param (6);

	timeF = ProcTime();
	ChanIn(KANAL4WE,table,4*ALPHA_F);
	timeF = ProcTime() - timeF;

	ChanOutInt(KANAL1WY,(int)table);

	timeG = ProcTime();
	ChanIn(KANAL4WE,&table[ALPHA_F],4*ALPHA_G);
	timeG = ProcTime() - timeG;

	timeToG = ProcTime();
	ChanOut(KANAL6WY,&table[ALPHA_F],4*ALPHA_G);
	timeToG = ProcTime() - timeToG;

	data.time = timeF;
	data.sender = 'A';
	data.reciever = 'F';
	data.processor = 'F';
	data.type = SEND;
	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	data.time = timeG;
	data.sender = 'A';
	data.reciever = 'F';
	data.processor = 'G';
	data.type = SEND;
	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	data.time = timeToG;
	data.sender = 'F';
	data.reciever = 'G';
	data.processor = 'G';
	data.type = SEND;
	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL4WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL4WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL2WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	return 0;
}
