/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"
int main()
{
	Channel*  KANAL2WY;
	Channel*  KANAL3WE;
	Channel*  KANAL6WE;
	Channel*  KANAL5WY;

	struct time_data data;

	int timeH = 0, timeI = 0, *table;

	KANAL2WY   = (Channel *)  get_param (1);
	KANAL3WE   = (Channel *)  get_param (2);
	KANAL6WE   = (Channel *)  get_param (3);
	KANAL5WY   = (Channel *)  get_param (4);

	table = (int*) ChanInInt(KANAL6WE);

	timeH = ProcTime();
	ChanOut(KANAL2WY,&table[ALPHA_A + ALPHA_B + ALPHA_C+ ALPHA_D + ALPHA_E + ALPHA_F + ALPHA_G], 4*ALPHA_H);
	timeH = ProcTime() - timeH;

	timeI = ProcTime();
	ChanOut(KANAL2WY,&table[ALPHA_A + ALPHA_B + ALPHA_C + ALPHA_D + ALPHA_E + ALPHA_F + ALPHA_G + ALPHA_H], 4*ALPHA_I);
	timeI = ProcTime() - timeI;

	data.time = timeH;
	data.sender = 'A';
	data.reciever = 'H';
	data.processor = 'H';
	data.type = SEND;
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	data.time = timeI;
	data.sender = 'A';
	data.reciever = 'H';
	data.processor = 'I';
	data.type = SEND;
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));



	return 0;
}
