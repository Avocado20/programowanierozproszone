/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"

int main()
{
	Channel*  KANAL6WY;
	Channel*  KANAL5WE;

	int *table, time, i, eval;
	struct time_data data;

	KANAL6WY   = (Channel *)  get_param (1);
	KANAL5WE   = (Channel *)  get_param (2);

	table = (int*) ChanInInt(KANAL5WE);
	
	time = ProcTime();
    for (i = 0; i <ALPHA_D; i++)
		eval = compute(table[i]);
	time = ProcTime() - time;

	data.time = time;
	data.processor = 'D';
	data.type = COMPUTE;
	ChanOut(KANAL6WY,&data,sizeof(struct time_data));


	return 0;
}
