/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"

int main()
{
	Channel*  KANAL4WY;
	Channel*  KANAL5WE;
	Channel*  KANAL6WE;

	struct time_data data;

	int timeD = 0, timeE = 0, *table;

    
	KANAL4WY   = (Channel *)  get_param (1);
	KANAL5WE   = (Channel *)  get_param (2);
	KANAL6WE   = (Channel *)  get_param (3);

	table = (int*) ChanInInt(KANAL6WE);

	timeD = ProcTime();
	ChanOut(KANAL4WY,&table[ALPHA_A + ALPHA_B +ALPHA_C], 4*ALPHA_D);
	timeD = ProcTime() - timeD;

	timeE = ProcTime();
	ChanOut(KANAL4WY,&table[ALPHA_A + ALPHA_B +ALPHA_C+ALPHA_D], 4*ALPHA_E);
	timeE = ProcTime() - timeD;

	data.time = timeD;
	data.sender = 'A';
	data.reciever = 'D';
	data.processor = 'D';
	data.type = SEND;
	ChanOut(KANAL4WY,&data,sizeof(struct time_data));

	data.time = timeE;
	data.sender = 'A';
	data.reciever = 'D';
	data.processor = 'E';
	data.type = SEND;
	ChanOut(KANAL4WY,&data,sizeof(struct time_data));

	return 0;
}
