/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"

int main()
{
	Channel*  KANAL6WE;
	Channel*  KANAL5WY;
	int *table, time, i, eval;
	struct time_data data;

	KANAL6WE   = (Channel *)  get_param (1);
	KANAL5WY   = (Channel *)  get_param (2);

	table = (int*) ChanInInt(KANAL6WE);
	
	time = ProcTime();
    for (i = 0; i <ALPHA_F; i++)
		eval = compute(table[i]);
	time = ProcTime() - time;

	data.time = time;
	data.processor = 'F';
	data.type = COMPUTE;
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));


	return 0;
}
