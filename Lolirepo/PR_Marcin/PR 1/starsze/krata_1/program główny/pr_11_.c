/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"

int main()
{
	Channel*  KANAL4WE;
	Channel*  KANAL5WY;

	int table[200000], i, eval, sendTime, computeTime;

	struct time_data data;

	KANAL4WE   = (Channel *)  get_param (1);
	KANAL5WY   = (Channel *)  get_param (2);

	sendTime = ProcTime();
	ChanIn(KANAL4WE,table,4*ALPHA_I);
	sendTime = ProcTime() - sendTime;

	computeTime = ProcTime();
	for (i = 0 ; i < ALPHA_I; i++)
		eval = compute(table[i]);
	computeTime = ProcTime() - computeTime;

	data.time = sendTime;
	data.sender = 'H';
	data.reciever = 'I';
	data.processor = 'I';
	data.type = SEND;
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	data.time = computeTime;
	data.processor = 'I';
	data.type = COMPUTE;
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	return 0;
}
