/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <stdio.h>
#include <process.h>
#include <channel.h>
#include <misc.h>
#include <stdlib.h>

#include "common.h"

void print_times(struct time_data data){
	if (data.type == COMPUTE)
		printf("Obliczenia dla %c: %d\n", data.processor, data.time);
	else 
		printf("Przesylanie miedzy %c a %c, paczka dla %c: %d\n",data.sender,data.reciever,data.processor,data.time);
}

int main()
{
	Channel*  KANAL4WE;
	Channel*  KANAL3WY;
	Channel*  KANAL5WY;
	Channel*  KANAL6WE;

	struct time_data *data;

	int *table;

	int i,eval, time, time2;

	KANAL4WE   = (Channel *)  get_param (3);
	KANAL3WY   = (Channel *)  get_param (4);
	KANAL5WY   = (Channel *)  get_param (5);
	KANAL6WE   = (Channel *)  get_param (6);

	data = malloc(sizeof(struct time_data));

	table = (int*)ChanInInt(KANAL4WE);
	
	time = ProcTime();
	for (i = 0; i < ALPHA_B; i++)
		eval = compute(table[i]);

	time2 = ProcTime();
	
	printf("Obliczenia dla %c: %d\n", 'B', time2 - time);
	
	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	
	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	
	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	
	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL6WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL6WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL6WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL6WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL6WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL6WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL6WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL6WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	ChanIn(KANAL4WE,data,sizeof(struct time_data));
	print_times(*data);

	printf("alfaA: %d, alfaB: %d, alfaC: %d", ALPHA_A, ALPHA_B, ALPHA_C);

	exit_terminate (0);
	return 0;
}
