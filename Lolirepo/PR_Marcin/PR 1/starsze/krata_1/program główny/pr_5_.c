/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"
int main()
{
	Channel*  KANAL5WE;
	Channel*  KANAL4WY;

	struct time_data data;

	int i, time, eval;

	int *table;

	KANAL5WE   = (Channel *)  get_param (1);
	KANAL4WY   = (Channel *)  get_param (2);

	table = (int*) ChanInInt(KANAL5WE);
	
	
		
	time = ProcTime();
	for (i = 0; i < ALPHA_A; i++){
		eval = compute(table[i]);
	}
	

	time = ProcTime() - time;

	data.time = time;
	data.type = COMPUTE;
	data.processor = 'A';

	ChanOut(KANAL4WY,&data,sizeof(data));

	return 0;
}
