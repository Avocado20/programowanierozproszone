/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include "common.h"


int main()
{
	Channel*  KANAL6WE;
	Channel*  KANAL5WY;
	Channel*  KANAL0WY;
	Channel*  KANAL7WE;
	Channel*  KANAL1WY;
	Channel*  KANAL2WE;

	int table[300000], timeH, timeI, timeToI;
	struct time_data data;

	KANAL6WE   = (Channel *)  get_param (1);
	KANAL5WY   = (Channel *)  get_param (2);
	KANAL0WY   = (Channel *)  get_param (3);
	KANAL7WE   = (Channel *)  get_param (4);
	KANAL1WY   = (Channel *)  get_param (5);
	KANAL2WE   = (Channel *)  get_param (6);

	timeH = ProcTime();
	ChanIn(KANAL6WE,table,4*ALPHA_H);
	timeH = ProcTime() - timeH;

	ChanOutInt(KANAL1WY,(int)table);

	timeI = ProcTime();
	ChanIn(KANAL6WE,&table[ALPHA_H],4*ALPHA_I);
	timeI = ProcTime() - timeI;

	timeToI = ProcTime();
	ChanOut(KANAL0WY,&table[ALPHA_H],4*ALPHA_I);
	timeToI = ProcTime() - timeToI;

	data.time = timeH;
	data.sender = 'A';
	data.reciever = 'H';
	data.processor = 'H';
	data.type = SEND;
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	data.time = timeI;
	data.sender = 'A';
	data.reciever = 'H';
	data.processor = 'I';
	data.type = SEND;
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	data.time = timeToI;
	data.sender = 'H';
	data.reciever = 'I';
	data.processor = 'I';
	data.type = SEND;
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	ChanIn(KANAL7WE,&data,sizeof(struct time_data));
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	ChanIn(KANAL7WE,&data,sizeof(struct time_data));
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	ChanIn(KANAL2WE,&data,sizeof(struct time_data));
	ChanOut(KANAL5WY,&data,sizeof(struct time_data));

	return 0;
}
