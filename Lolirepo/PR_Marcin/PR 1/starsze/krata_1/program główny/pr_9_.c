/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>

#include "common.h"

int main()
{
	Channel*  KANAL3WE;
	Channel*  KANAL2WY;
	Channel*  KANAL5WE;
	Channel*  KANAL6WY;

	int table[200000], i, eval, sendTime, computeTime;

	struct time_data data;

	KANAL3WE   = (Channel *)  get_param (1);
	KANAL2WY   = (Channel *)  get_param (2);
	KANAL5WE   = (Channel *)  get_param (3);
	KANAL6WY   = (Channel *)  get_param (4);

	sendTime = ProcTime();
	ChanIn(KANAL3WE,table,4*ALPHA_G);
	sendTime = ProcTime() - sendTime;

	computeTime = ProcTime();
	for (i = 0 ; i < ALPHA_G; i++)
		eval = compute(table[i]);
	computeTime = ProcTime() - computeTime;

	data.time = sendTime;
	data.sender = 'F';
	data.reciever = 'G';
	data.processor = 'G';
	data.type = SEND;
	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	data.time = computeTime;
	data.processor = 'G';
	data.type = COMPUTE;
	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	ChanIn(KANAL3WE,&data,sizeof(struct time_data));

	ChanOut(KANAL6WY,&data,sizeof(struct time_data));

	return 0;
}
