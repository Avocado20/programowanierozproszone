/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include <stdlib.h>

float licz(float x){
	return x*x*x*x*x*x*x*x
			-x*x*x*x*x*x*x
			  +x*x*x*x*x*x
				-x*x*x*x*x
				  +x*x*x*x
					-x*x*x
					  +x*x
						-x;
}

int main()
{
	Channel*  KANAL6WE;
	Channel*  KANAL5WY;

	float* tab;
	int i, j, czas;

	KANAL6WE   = (Channel *)  get_param (1);
	KANAL5WY   = (Channel *)  get_param (2);

	tab = (float*)malloc(100000 * sizeof(float));

	for (i = 5000; i <= 100000; i += 5000){
		czas = ProcTime();

		for (j = 0; j < i; j++)
			licz(j);

		ChanOutInt(KANAL5WY, ProcTime()-czas);
	}

	free(tab);

	return 0;
}
