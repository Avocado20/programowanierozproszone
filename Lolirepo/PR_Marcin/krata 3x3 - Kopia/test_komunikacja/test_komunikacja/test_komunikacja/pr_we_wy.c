/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <stdio.h>
#include <process.h>
#include <channel.h>
#include <misc.h>
#include <stdlib.h>

int main()
{
	Channel*  KANAL2WY;
	Channel*  KANAL3WE;
	Channel*  KANAL4WY;
	Channel*  KANAL5WE;

	int* tab;
	int i, czas;

	KANAL2WY   = (Channel *)  get_param (3);
	KANAL3WE   = (Channel *)  get_param (4);
	KANAL4WY   = (Channel *)  get_param (5);
	KANAL5WE   = (Channel *)  get_param (6);

	tab = (int*)malloc(100000 * sizeof(int));

	printf("TEST LACZA POZIOMEGO\n");

	for (i = 0; i < 100000; i++)
		tab[i] = i;

	for (i = 5000; i <= 100000; i += 5000){
		czas = ProcTime();
		ChanOut(KANAL2WY, tab, i*sizeof(int));
		ChanIn(KANAL3WE, tab, i*sizeof(int));
		printf("czas dla %d: %d\n", i, ProcTime()-czas);
	}

	printf("TEST LACZA PIONOWEGO\n");

	for (i = 5000; i <= 100000; i += 5000){
		czas = ProcTime();
		ChanOut(KANAL4WY, tab, i*sizeof(int));
		ChanIn(KANAL5WE, tab, i*sizeof(int));
		printf("czas dla %d: %d\n", i, ProcTime()-czas);
	}

	printf("KONIEC\n");

	/*dane = 300;
	ChanOut(KANAL4WY, &dane, sizeof(int));
	dane = 400;
	printf("dane: %d\n", dane);
	ChanIn(KANAL5WE, &dane, sizeof(int));
	printf("dane: %d\n", dane);*/

	free(tab);

	exit_terminate (0);
	return 0;
}
