/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include <stdlib.h>

int main()
{
	Channel*  KANAL6WE;
	Channel*  KANAL5WY;

	int* tab;
	int i;

	KANAL6WE   = (Channel *)  get_param (1);
	KANAL5WY   = (Channel *)  get_param (2);

	tab = (int*)malloc(100000 * sizeof(int));

	for (i = 5000; i <= 100000; i += 5000){
		ChanIn(KANAL6WE, tab, i*sizeof(int));
		ChanOut(KANAL5WY, tab, i*sizeof(int));
	}

	free(tab);

	return 0;
}
