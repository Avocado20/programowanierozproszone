/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include <stdlib.h>

int main()
{
	Channel*  KANAL6WE;
	Channel*  KANAL5WY;

	int S = 5;
	int Z = 25000;
	int k = Z / 20;

	float* tab;
	int i;

	KANAL6WE   = (Channel *)  get_param (1);
	KANAL5WY   = (Channel *)  get_param (2);

	tab = (float*)malloc(S*Z * sizeof(float));

	for (i = k; i <= Z; i += k){
		ChanIn(KANAL6WE, tab, S*i*sizeof(float));
		ChanOut(KANAL5WY, tab, S*i*sizeof(float));
	}

	free(tab);

	return 0;
}
