/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <stdio.h>
#include <process.h>
#include <channel.h>
#include <misc.h>
#include <stdlib.h>

int main()
{
	Channel*  KANAL2WY;
	Channel*  KANAL3WE;
	Channel*  KANAL4WY;
	Channel*  KANAL5WE;

	int S = 5;
	int Z = 25000;
	int k = Z / 20;

	float* tab;
	int i, czas;

	KANAL2WY   = (Channel *)  get_param (3);
	KANAL3WE   = (Channel *)  get_param (4);
	KANAL4WY   = (Channel *)  get_param (5);
	KANAL5WE   = (Channel *)  get_param (6);

	tab = (float*)malloc(S*Z * sizeof(float));

	for (i = 0; i < S*Z; i++)
		tab[i] = rand();


	printf("TEST LACZA POZIOMEGO\n");

	for (i = k; i <= Z; i += k){
		czas = ProcTime();
		ChanOut(KANAL2WY, tab, S*i*sizeof(float));
		ChanIn(KANAL3WE, tab, S*i*sizeof(float));
		printf("czas dla %d: %d\n", i, ProcTime()-czas);
	}

	printf("TEST LACZA PIONOWEGO\n");

	for (i = k; i <= Z; i += k){
		czas = ProcTime();
		ChanOut(KANAL4WY, tab, S*i*sizeof(float));
		ChanIn(KANAL5WE, tab, S*i*sizeof(float));
		printf("czas dla %d: %d\n", i, ProcTime()-czas);
	}

	printf("KONIEC\n");

	free(tab);

	exit_terminate (0);
	return 0;
}
