/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include <stdlib.h>

int main()
{
	Channel*  KANAL0WE;
	Channel*  KANAL7WY;

	int S = 5;
	int Z = 25000;
	int k = Z / 20;

	float* tab;
	int i;

	KANAL0WE   = (Channel *)  get_param (1);
	KANAL7WY   = (Channel *)  get_param (2);

	tab = (float*)malloc(S*Z * sizeof(float));

	for (i = k; i <= Z; i += k){
		ChanIn(KANAL0WE, tab, S*i*sizeof(float));
		ChanOut(KANAL7WY, tab, S*i*sizeof(float));
	}

	free(tab);

	return 0;
}
