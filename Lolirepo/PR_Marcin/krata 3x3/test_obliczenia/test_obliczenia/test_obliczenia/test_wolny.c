/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <process.h>
#include <channel.h>
#include <misc.h>
#include <stdlib.h>

float licz(float x, float w1, float w2, float w3, float w4){
	return  w1*w3*x*x*x*x*x*x*x - w2*w2*w2*x*x*x*x*x*x*x - w4*w1*w3*x*x*x*x*x*x*x + w2*w4*w3*x*x*x*x*x*x*x
			- w4*x*x*x*x*x*x - w1*w3*x*x*x*x*x*x - w2*w4*w1*x*x*x*x*x*x + w2*w2*x*x*x*x*x*x - w3*w1*w2*x*x*x*x*x*x
			- w1*w2*x*x*x*x*x + w2*w3*x*x*x*x*x - w4*w1*x*x*x*x*x + w4*w3*w1*x*x*x*x*x + w4*w3*w4*x*x*x*x*x - w1*w2*w1*x*x*x*x*x
			+ w1*x*x*x*x - w2*w1*x*x*x*x - w4*w3*w3*x*x*x*x + w2*x*x*x*x + w4*x*x*x*x - w3*w2*x*x*x*x + w1*x*x*x*x - w3*x*x*x*x
			+ w2*w4*x*x*x - w1*x*x*x + w4*w1*x*x*x - w3*x*x*x - w3*w3*x*x*x + w2*x*x*x - w4*x*x*x
			- w4*x*x + w3*w3*x*x + w2*x*x - w1*w3*w3*x*x + w4*x*x - w3*w4*x*x + w1*x*x - w3*x
			- w3*x + w2*x + w4*w1*x - w1*x + w2*w2*x + w1*w3*x - w3*w2*x;
}

int main()
{
	Channel*  KANAL6WE;
	Channel*  KANAL5WY;

	int S = 5;
	int Z = 25000;
	int k = Z / 20;

	float* tab;
	int i, j, czas;

	KANAL6WE   = (Channel *)  get_param (1);
	KANAL5WY   = (Channel *)  get_param (2);

	tab = (float*)malloc(S*Z * sizeof(float));

	for (i = 0; i < S*Z; i++)
		tab[i] = rand();


	for (i = k; i <= Z; i += k){
		czas = ProcTime();

		for (j = 0; j < i; j++)
			licz(tab[S*j], tab[S*j+1], tab[S*j+2], tab[S*j+3], tab[S*j+4]);

		ChanOutInt(KANAL5WY, ProcTime()-czas);
	}

	free(tab);

	return 0;
}
