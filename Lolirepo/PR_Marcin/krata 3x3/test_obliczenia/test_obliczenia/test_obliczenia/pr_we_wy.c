/* --------------------------------------------------- *
 *   Szkielet pliku zrodlowego wygenrowany przez       *
 *                 TRANSPUTER STUDIO                   *
 * --------------------------------------------------- */

#include <stdio.h>
#include <process.h>
#include <channel.h>
#include <misc.h>
#include <stdlib.h>

int main()
{
	Channel*  KANAL2WY;
	Channel*  KANAL3WE;
	Channel*  KANAL4WY;
	Channel*  KANAL5WE;

	int S = 5;
	int Z = 25000;
	int k = Z / 20;

	float* tab;
	int i, czas;

	KANAL2WY   = (Channel *)  get_param (3);
	KANAL3WE   = (Channel *)  get_param (4);
	KANAL4WY   = (Channel *)  get_param (5);
	KANAL5WE   = (Channel *)  get_param (6);

	tab = (float*)malloc(S*Z * sizeof(float));

	for (i = 0; i < Z; i++)
		tab[i] = i;

	printf("TEST TRANSPUTERA WOLNEGO\n");

	for (i = k; i <= Z; i += k){
		printf("czas dla %d: %d\n", i, ChanInInt(KANAL3WE));
	}

	printf("TEST TRANSPUTERA SZYBKIEGO\n");

	for (i = k; i <= Z; i += k){
		printf("czas dla %d: %d\n", i, ChanInInt(KANAL5WE));
	}

	printf("KONIEC\n");

	/*dane = 300;
	ChanOut(KANAL4WY, &dane, sizeof(int));
	dane = 400;
	printf("dane: %d\n", dane);
	ChanIn(KANAL5WE, &dane, sizeof(int));
	printf("dane: %d\n", dane);*/

	free(tab);

	exit_terminate (0);
	return 0;
}
